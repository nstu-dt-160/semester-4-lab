﻿using System.Collections.Generic;
using SDI_application.Core.Model;

namespace SDI_application.Core.Collection;

/// <summary>
/// Object list class
/// </summary>
public class ItemList
{
    private static ItemList? _instance;

    private List<FurnitureItem>? List;

    /// <summary>
    /// Base private constructor
    /// </summary>
    private ItemList()
    {
        List = new List<FurnitureItem>();
    }

    /// <summary>
    /// Get singleton
    /// </summary>
    /// <returns></returns>
    public static ItemList GetItemList()
    {
        return _instance ??= new ItemList();
    }

    /// <summary>
    /// Add to list
    /// </summary>
    /// <param name="obj"></param>
    public void Add(FurnitureItem obj)
    {
        List?.Add(obj);
    }

    /// <summary>
    /// Remove by number
    /// </summary>
    /// <param name="number"></param>
    public void Remove(int number)
    {
        List?.RemoveAt(number);
    }

    /// <summary>
    /// Get by number
    /// </summary>
    /// <param name="number"></param>
    /// <returns></returns>
    public FurnitureItem? GetByNumber(int number)
    {
        return List?[number];
    }

    /// <summary>
    /// Get size of list
    /// </summary>
    /// <returns></returns>
    public int? GetCount()
    {
        return List?.Count;
    }
}