﻿namespace SDI_application.Core.Exceptions;

using System;

/// <summary>
/// NoArticleException class
/// </summary>
public class NoArticleException : Exception
{
    /// <summary>
    /// Base constructor
    /// </summary>
    /// <param name="message"></param>
    public NoArticleException(string message) : base(message)
    {
    }
}