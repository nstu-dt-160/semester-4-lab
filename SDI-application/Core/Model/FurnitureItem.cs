﻿namespace SDI_application.Core.Model;

using Interfaces;

/// <summary>
/// Furniture item class 
/// </summary>
public class FurnitureItem : AbstractProduct, IStringable
{
    /// <summary>
    /// Furniture type
    /// </summary>
    protected string? Type { get; set; }

    /// <summary>
    /// Furniture equipment
    /// </summary>
    protected string? Equipment { get; set; }

    /// <summary>
    /// Base constructor
    /// </summary>
    /// <param name="name"></param>
    /// <param name="article"></param>
    /// <param name="type"></param>
    /// <param name="equipment"></param>
    public FurnitureItem(
        string? name,
        string? article,
        string? type,
        string? equipment
    )
    {
        Name = name;
        Article = article;
        Type = type;
        Equipment = equipment;
    }

    public string GetAsString()
    {
        return "Name: " + (Name == "" ? "Null" : Name)
                        + "; Article: " + (Article == "" ? "Null" : Article)
                        + "; Type: " + (Type == "" ? "Null" : Type)
                        + "; Equipment: " + (Equipment == "" ? "Null" : Equipment);
    }
}