﻿namespace SDI_application.Core.Model;

/// <summary>
/// Abstract model-class for Product entity
/// </summary>
public class AbstractProduct
{
    /// <summary>
    /// Product name
    /// </summary>
    protected string? Name { get; set; }

    /// <summary>
    /// Product article
    /// </summary>
    protected string? Article { get; set; }

    /// <summary>
    /// Get full product name
    /// </summary>
    /// <returns></returns>
    public string GetFullName()
    {
        return Name != null
            ? "Наименование продукта: " + Name
            : "Наименование продукта не задано";
    }
}