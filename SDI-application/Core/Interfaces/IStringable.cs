﻿namespace SDI_application.Core.Interfaces;

/// <summary>
/// Stringable interface
/// </summary>
public interface IStringable
{
    /// <summary>
    /// Get object as string
    /// </summary>
    /// <returns></returns>
    public string GetAsString();
}