﻿namespace SDI_application;

using System;
using Core.Collection;
using Core.Exceptions;
using Core.Model;
using System.Windows;

/// <summary>
/// Interaction logic for MainWindow.xaml
/// </summary>
public partial class MainWindow : Window
{
    /// <summary>
    /// Constructor
    /// </summary>
    public MainWindow()
    {
        InitializeComponent();
    }

    /// <summary>
    /// Create item
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="args"></param>
    /// <exception cref="NoArticleException"></exception>
    private void CreateItem(object sender, RoutedEventArgs args)
    {
        try
        {
            var article = ArticleInput.Text;

            if (article == "")
            {
                throw new NoArticleException("Артикул не может быть пустым");
            }

            var name = NameInput.Text;
            var type = TypeInput.Text;
            var equipment = EquipmentInput.Text;

            var obj = new FurnitureItem(name, article, type, equipment);

            var list = ItemList.GetItemList();
            list.Add(obj);
            productsList.Items.Add(obj.GetAsString());
        }
        catch (Exception exception)
        {
            MessageBox.Show("Ошибка: " + exception.Message);
        }
    }

    /// <summary>
    /// Remove item
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="args"></param>
    private void DeleteItem(object sender, RoutedEventArgs args)
    {
        if (productsList.SelectedItems.Count > 0)
        {
            var index = productsList.SelectedIndex;
            productsList.Items.RemoveAt(index);
            var list = ItemList.GetItemList();
            list.Remove(index);
        }
    }
}